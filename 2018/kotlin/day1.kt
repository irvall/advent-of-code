data class Chris(val dumb: Boolean, val age: Int = 16)

fun main() {
    val time = elapsedTimeMillis {
        val person = Chris(dumb = true, age = 12)
        println(person)
    }

    println(time)
}
