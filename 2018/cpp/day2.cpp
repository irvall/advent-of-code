#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {

	string line;
	ifstream input("input.txt");
	int twos, threes;
	twos = threes = 0;
	if(input.is_open()) {
		while(getline(input, line)) {
			int alph[26] = { 0 };
			bool l2, l3;
			for(int i = 0; i < line.size(); i++) {
				switch(++alph[line[i] - 'a']) {
					case 1: break;
					case 2:
						l2 = true;
						break;
					case 3:
						l3 = true;
						break;
							
				}
			}
			if(l2) twos++;
			if(l3) threes++;
		}
		input.close();
	}
	cout << (twos*threes);
	return 0;
}
