#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

int day1_1() { 
	FILE *fp;
	fp = fopen("day1.txt", "r");
	int i,c,sum;
	i = sum = 0;
	char n[8];
	while ((c = fgetc(fp)) != EOF) {
		if(c == '\n') { 
			n[i] = 0;
			sum += atoi(n);
			i = 0;
		} else n[i++] = c;
	}
	return sum; 
}

unsigned u_con(int a) {
	unsigned u = a;
	return u;
}

int day1_2() { 
	FILE *fp;
	fp = fopen("day1.txt", "r");
	int i,c,sum;
	i = sum = 0;
	char n[8];
	int *visited = calloc(100000, sizeof(int));
	int fix = 1000;
	int min = INT_MAX;
	int max = INT_MIN;
	while ((c = fgetc(fp)) != EOF) {
		if(c == '\n') { 
			n[i] = 0;
			sum += atoi(n);
			if(sum > max) max = sum;
			if(min > sum) min = sum;
			if(visited[sum+fix]++ == 1) return sum;
			i = 0;
		} else n[i++] = c;
	}
	return -666;
}

void view_signed(int a) {
	for(int i = 31; i >= 0; i--) {
		printf("%d",(a>>i)&1);
	}
	printf("\n");
}



int main() {
	printf("%d\n", day1_2());
	return 0;
}