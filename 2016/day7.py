import re

def isABBA(s):
    pattern = re.compile(r'(.)(?!\1)(.)\2\1')
    matches = re.findall(pattern, s)
    return len(matches) > 0

def isTLS(s):
    pattern = re.compile(r'(\w+)\[(\w+)\](\w+)')
    l, m, r = re.findall(pattern, s)[0]
    print(l,m,r)
    return (not isABBA(m) and (isABBA(l) or isABBA(r)))

with open('day7_input.txt', 'r') as file:

    supports = 0
    for line in file:
        if isTLS(line.strip()):
            supports += 1

    print(supports)