package main

import (
	"fmt"
	"sort"
	"strings"
)

type sortRunes []rune

func (s sortRunes) Less(i, j int) bool {
	return s[i] < s[j]
}

func (s sortRunes) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortRunes) Len() int {
	return len(s)
}

func SortString(s string) string {
	r := []rune(s)
	sort.Sort(sortRunes(r))
	return string(r)
}

func main() {
	s := "aaaaa-bbb-z-y-x-123[abxyz]"
	input := strings.Split(s, "[")
	token := input[1][:len(input[1])-1]
	name := strings.Split(input[0], "-")
	name = name[:len(name)-1]
	newname := strings.Join(name, "")
	fmt.Println(token)
	fmt.Println(name)
}
