import hashlib

def hash(base):
    i = 0
    ut = 'utf-8'
    zrs = '00000'
    d = hashlib.md5(base.encode('utf-8'))
    k = 0
    while k < 8:
        i += 1
        digest = d.copy()
        digest.update(('%d' % i).encode('utf-8'))
        v = digest.hexdigest()
        if v.startswith(zrs):
            print(v[5], end ='')
            k += 1
    print()

hash('abc')
# python3 day5.py  9.87s user 0.01s system 99% cpu 9.886 total