import hashlib
import io

def hash(base):
    i = -1
    ut = 'utf-8'
    zrs = '00000'
    pwd = ['_' for _ in range(8)]
    k = 0
    m = hashlib.md5()
    m.update(base.encode('utf-8'))
    while k < 8:
        md = m.copy()
        md.update(str(i).encode('utf-8'))
        v = md.hexdigest()
        i += 1
        if v.startswith(zrs):
            c = ord(v[5])
            if c >= 48 and c <= 55:
                j = c - 48
                if pwd[j] != '_': 
                    continue
                pwd[j] = v[6]
                k += 1
                print (''.join(pwd))
    return pwd

print(hash('ugkcyxxp'))