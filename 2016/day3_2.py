def validTri(a, b, c):
    return (a + b) > c and (a + c) > b and (b + c) > a

f = open("day3_input.txt", "r")

counter = 0
for line in f:
    if validTri(*map(int, line.split())):
        counter += 1

print(counter)