import re

# Sorts a string according to rules (first frequency, ties are alphabetic)
def sortString(s):
    sortedArray = sorted(s)
    n = len(sortedArray)
    i = 0
    lenTups = []
    while i < n:
        c = sortedArray[i]
        j = i + 1
        while j < n and c == sortedArray[j]:
            j += 1
        lenTups.append((c, j - i))
        i = j
    return ''.join(map(lambda y: y[0], sorted(lenTups, key=lambda y: y[1], reverse=True)))


f = open("day4_input.txt", "r")

pattern = re.compile(r'^(?:([\w-]*)-(\d+)\[(\w+)\])$')
sectorIdSum = 0
for line in f:
    name, sId, chksum = re.findall(pattern, line)[0]
    key = sortString(name.replace('-', ''))
    if key.startswith(chksum):
        sectorIdSum += int(sId)

print(sectorIdSum)