def sort_getchar(ls):
    ls = sorted(ls, key = lambda y: y[1], reverse = True)
    c, _ = ls[0]
    return c

with open("day6_input.txt", "r") as file:
    freq = [[(chr(i), 0) for i in range(97, 123)] for _ in range(8)]
    for line in map(lambda s: s.strip(), file):
        for i in range(len(line)):
            c, f = freq[i][ord(line[i])-97] 
            freq[i][ord(line[i])-97] = c, f + 1

    print(''.join(map(sort_getchar, freq)))