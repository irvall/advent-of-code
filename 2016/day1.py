xs = 'R4,R4,L1,R3,L5,R2,R5,R1,L4,R3,L5,R2,L3,L4,L3,R1,R5,R1,L3,L1,R3,L1,R2,R2,L2,R5,L3,L4,R4,R4,R2,L4,L1,R5,L1,L4,R4,L1,R1,L2,R5,L2,L3,R2,R1,L194,R2,L4,R49,R1,R3,L5,L4,L1,R4,R2,R1,L5,R3,L5,L4,R4,R4,L2,L3,R78,L5,R4,R191,R4,R3,R1,L2,R1,R3,L1,R3,R4,R2,L2,R1,R4,L5,R2,L2,L4,L2,R1,R2,L3,R5,R2,L3,L3,R3,L1,L1,R5,L4,L4,L2,R5,R1,R4,L3,L5,L4,R5,L4,R5,R4,L3,L2,L5,R4,R3,L3,R1,L5,R5,R1,L3,R2,L5,R5,L3,R1,R4,L5,R4,R2,R3,L4,L5,R3,R4,L5,L5,R4,L4,L4,R1,R5,R3,L1,L4,L3,L4,R1,L5,L1,R2,R2,R4,R4,L5,R4,R1,L1,L1,L3,L5,L2,R4,L3,L5,L4,L1,R3'.split(',')
ys = 'R8,R4,R4,R8'.split(',')

def makeTurn(currentlyFacing, direction):
    if currentlyFacing == 'N':
        return 'E' if direction == 'R' else 'W'
    elif currentlyFacing == 'E':
        return 'S' if direction == 'R' else 'N'
    elif currentlyFacing == 'S':
        return 'W' if direction == 'R' else 'E'
    elif currentlyFacing == 'W':
        return 'N' if direction == 'R' else 'S'

def run():
    facing = 'N'
    visited = []
    state = [0, 0]
    for x in xs:
        direction, steps = x[0], int(x[1:])
        facing = makeTurn(facing, direction)
        if facing == 'E':
            for i in range(1, steps + 1):
                state[0] += 1
                if state in visited:
                    return state
                visited.append(state.copy())
        elif facing == 'W':
            for i in range(1, steps + 1):
                state[0] -= 1 
                if state in visited:
                    return state
                visited.append(state.copy())
        elif facing == 'N':
            for i in range(1, steps + 1):
                state[1] += 1 
                if state in visited:        
                    return state
                visited.append(state.copy())
        elif facing == 'S':
            for i in range(1, steps + 1):
                state[1] -= 1 
                if state in visited:
                    return state
                visited.append(state.copy())
        visited.append(state.copy())

print(sum(map(abs, run())))
