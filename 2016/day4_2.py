import re

def shift(s, times):
    return ''.join(map(lambda y: chr((ord(y)-97 + times) % 26 + 97), s))

f = open("day4_input.txt", "r")

pattern = re.compile(r'^(?:([\w-]*)-(\d+)\[(\w+)\])$')

for line in f:
    name, sId, _ = re.findall(pattern, line)[0]
    key = shift(name, int(sId))
    if key.__contains__("north"):
        print(sId)