cookie = """
LURLDDLDULRURDUDLRULRDLLRURDUDRLLRLRURDRULDLRLRRDDULUDULURULLURLURRRLLDURURLLUURDLLDUUDRRDLDLLRUUDURURRULURUURLDLLLUDDUUDRULLRUDURRLRLLDRRUDULLDUUUDLDLRLLRLULDLRLUDLRRULDDDURLUULRDLRULRDURDURUUUDDRRDRRUDULDUUULLLLURRDDUULDRDRLULRRRUUDUURDULDDRLDRDLLDDLRDLDULUDDLULUDRLULRRRRUUUDULULDLUDUUUUDURLUDRDLLDDRULUURDRRRDRLDLLURLULDULRUDRDDUDDLRLRRDUDDRULRULULRDDDDRDLLLRURDDDDRDRUDUDUUDRUDLDULRUULLRRLURRRRUUDRDLDUDDLUDRRURLRDDLUUDUDUUDRLUURURRURDRRRURULUUDUUDURUUURDDDURUDLRLLULRULRDURLLDDULLDULULDDDRUDDDUUDDUDDRRRURRUURRRRURUDRRDLRDUUULLRRRUDD
DLDUDULDLRDLUDDLLRLUUULLDURRUDLLDUDDRDRLRDDUUUURDULDULLRDRURDLULRUURRDLULUDRURDULLDRURUULLDLLUDRLUDRUDRURURUULRDLLDDDLRUDUDLUDURLDDLRRUUURDDDRLUDDDUDDLDUDDUUUUUULLRDRRUDRUDDDLLLDRDUULRLDURLLDURUDDLLURDDLULLDDDRLUDRDDLDLDLRLURRDURRRUDRRDUUDDRLLUDLDRLRDUDLDLRDRUDUUULULUDRRULUDRDRRLLDDRDDDLULURUURULLRRRRRDDRDDRRRDLRDURURRRDDULLUULRULURURDRRUDURDDUURDUURUURUULURUUDULURRDLRRUUDRLLDLDRRRULDRLLRLDUDULRRLDUDDUUURDUDLDDDUDL
RURDRUDUUUUULLLUULDULLLDRUULURLDULULRDDLRLLRURULLLLLLRULLURRDLULLUULRRDURRURLUDLULDLRRULRDLDULLDDRRDLLRURRDULULDRRDDULDURRRUUURUDDURULUUDURUULUDLUURRLDLRDDUUUUURULDRDUDDULULRDRUUURRRDRLURRLUUULRUDRRLUDRDLDUDDRDRRUULLLLDUUUULDULRRRLLRLRLRULDLRURRLRLDLRRDRDRLDRUDDDUUDRLLUUURLRLULURLDRRULRULUDRUUURRUDLDDRRDDURUUULLDDLLDDRUDDDUULUDRDDLULDDDDRULDDDDUUUURRLDUURULRDDRDLLLRRDDURUDRRLDUDULRULDDLDDLDUUUULDLLULUUDDULUUDLRDRUDLURDULUDDRDRDRDDURDLURLULRUURDUDULDDLDDRUULLRDRLRRUURRDDRDUDDLRRLLDRDLUUDRRDDDUUUDLRRLDDDUDRURRDDUULUDLLLRUDDRULRLLLRDLUDUUUUURLRRUDUDDDDLRLLULLUDRDURDDULULRDRDLUDDRLURRLRRULRL
LDUURLLULRUURRDLDRUULRDRDDDRULDLURDDRURULLRUURRLRRLDRURRDRLUDRUUUULLDRLURDRLRUDDRDDDUURRDRRURULLLDRDRDLDUURLDRUULLDRDDRRDRDUUDLURUDDLLUUDDULDDULRDDUUDDDLRLLLULLDLUDRRLDUUDRUUDUDUURULDRRLRRDLRLURDRURURRDURDURRUDLRURURUUDURURUDRURULLLLLUDRUDUDULRLLLRDRLLRLRLRRDULRUUULURLRRLDRRRDRULRUDUURRRRULDDLRULDRRRDLDRLUDLLUDDRURLURURRLRUDLRLLRDLLDRDDLDUDRDLDDRULDDULUDDLLDURDULLDURRURRULLDRLUURURLLUDDRLRRUUDULRRLLRUDRDUURLDDLLURRDLRUURLLDRDLRUULUDURRDULUULDDLUUUDDLRRDRDUDLRUULDDDLDDRUDDD
DRRDRRURURUDDDRULRUDLDLDULRLDURURUUURURLURURDDDDRULUDLDDRDDUDULRUUULRDUDULURLRULRDDLDUDLDLULRULDRRLUDLLLLURUDUDLLDLDRLRUUULRDDLUURDRRDLUDUDRULRRDDRRLDUDLLDLURLRDLRUUDLDULURDDUUDDLRDLUURLDLRLRDLLRUDRDUURDDLDDLURRDDRDRURULURRLRLDURLRRUUUDDUUDRDRULRDLURLDDDRURUDRULDURUUUUDULURUDDDDUURULULDRURRDRDURUUURURLLDRDLDLRDDULDRLLDUDUDDLRLLRLRUUDLUDDULRLDLLRLUUDLLLUUDULRDULDLRRLDDDDUDDRRRDDRDDUDRLLLDLLDLLRDLDRDLUDRRRLDDRLUDLRLDRUURUDURDLRDDULRLDUUUDRLLDRLDLLDLDRRRLLULLUDDDLRUDULDDDLDRRLLRDDLDUULRDLRRLRLLRUUULLRDUDLRURRRUULLULLLRRURLRDULLLRLDUUUDDRLRLUURRLUUUDURLRDURRDUDDUDDRDDRUD
"""

def move(num, direction):
    if direction == 'U': return num if num in [1, 2, 3] else num - 3
    if direction == 'D': return num if num in [7, 8, 9] else num + 3
    if direction == 'L': return num if num in [1, 4, 7] else num - 1
    if direction == 'R': return num if num in [3, 6, 9] else num + 1

##for code in cookie.strip().split('\n'):
##    n = 5
##    for c in code:
##        n = move(n, c)
##    print(n, end = '')
        
# Part 2
moves = {
            '1': [('3', 'D')],
            '2': [('6', 'D'), ('3', 'R')],
            '3': [('1', 'U'), ('2', 'L'), ('7', 'D'), ('4', 'R')],
            '4': [('3', 'L'), ('8', 'D')],
            '5': [('6', 'R')],
            '6': [('2', 'U'), ('7', 'R'), ('A', 'D'), ('5', 'L')],
            '7': [('3', 'U'), ('8', 'R'), ('B', 'D'), ('6', 'L')],
            '8': [('4', 'U'), ('9', 'R'), ('C', 'D'), ('7', 'L')],
            '9': [('8', 'L')],
            'A': [('6', 'U'), ('B', 'R')],
            'B': [('7', 'U'), ('C', 'R'), ('D', 'D'), ('A', 'L')],
            'C': [('8', 'U'), ('B', 'L')],
            'D': [('B', 'U')],
        }


small = """
ULL
RRDDD
LURDL
UUUUD
"""

def blankGrid(n):
    return ["_" for _ in range(n)]
#     1
#   2 3 4
# 5 6 7 8 9
#   A B C
#     D
oneBlank = ['_']
twoBlanks = ['_'] * 2
grid = [
    twoBlanks + ['1'] + twoBlanks,
    oneBlank  + ['2', '3', '4'] + oneBlank,
    [n for n in range(5, 10)],
    oneBlank + ['A', 'B', 'C'] + oneBlank,
    twoBlanks + ['D'] + twoBlanks
]

y, x = 2, 0

for code in cookie.strip().split('\n'):
    for c in code:
        if c == 'U': y -= 1
        if c == 'D': y += 1
        if c == 'L': x -= 1
        if c == 'R': x += 1
        
        if y == -1:
            y = 0

        if y > -1 && y < 5:
            if x > -1 && x < 5:
                print(grid[y][x])
        else:
            y
        ms = moves[n]
        for (dest,direc) in ms:
            if c == direc:
                n = dest
                break
    print(n, end = '')
print()
# Output = 9A7DC

print(grid[2][2])