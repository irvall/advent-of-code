cc = GCC
cflags = -g -Wall -Wextra -ftrapv -fverbose-asm -Wundef -Wshadow -Wwrite-strings -Wduplicated-cond -O3 -save-temps -S

build:
	cc hello.c -o hello --save-temps
