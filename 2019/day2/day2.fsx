open System.IO

let instr =
    File.ReadAllText "day2_input.txt"
    |> fun s -> s.Split(',') 
    |> Array.map int

let answer1 =
    instr.[1] <- 12
    instr.[2] <- 2
    let rec iter i (instr: int array) =
        let (v1,v2) = (instr.[instr.[i+1]], instr.[instr.[i+2]])
        match instr.[i] with
        | 1 -> instr.[instr.[i+3]] <- v1 + v2; iter (i+4) instr
        | 2 -> instr.[instr.[i+3]] <- v1 * v2; iter (i+4) instr
        | 99 -> ()
        | v -> failwithf "Expected opcode here, got %d!" v
    iter 0 (Array.copy instr)

let answer2 =
    for i in 0 .. 99 do
        for j in 0 .. 99 do
            let arrCopy = (Array.copy instr)
            instr.[1] <- i; instr.[2] <- j
            let rec iter i (instr: int array) =
                let (v1,v2) = (instr.[instr.[i+1]], instr.[instr.[i+2]])
                if instr.[i+3] = 0 then printfn "%d, %d" v1 v2
                match instr.[i] with
                | 1     -> 
                    instr.[instr.[i+3]] <- v1 + v2
                    if v1 + v2 = 19690720 then printfn "%d, %d" v1 v2
                    iter (i+4) instr
                | 2     -> 
                    instr.[instr.[i+3]] <- v1 * v2
                    if v1 * v2 = 19690720 then printfn "%d, %d" v1 v2
                    iter (i+4) instr
                | 99    -> ()
                | v -> failwithf "Expected opcode here, got %d!" v
            iter 0 (Array.copy instr)
