open System.IO


type instr = Add | Mul | Sto | Ret | Brk | Jit | Jif | Lt | Eq
and args = Single of int | Tuple of int * int | Triple of int * int * int


let getOpArgs (p: int array) i =
    let mode = fun v -> (v / 10000, v / 1000 % 10, v / 100 % 10, v % 100)
    let (third, second, first, op) = mode p.[i]
    match op with
    | 1     -> 
        let a1 = if first   = 1 then p.[i+1] else p.[p.[i+1]]
        let a2 = if second  = 1 then p.[i+2] else p.[p.[i+2]]
        let a3 = if third   = 1 then i+3     else p.[i+3]
        Add, Triple (a1,a2,a3)
    | 2     -> 
        let a1 = if first   = 1 then p.[i+1] else p.[p.[i+1]]
        let a2 = if second  = 1 then p.[i+2] else p.[p.[i+2]]
        let a3 = if third   = 1 then i+3     else p.[i+3]
        Mul, Triple (a1,a2,a3)
    | 3     -> 
        let a1 = if first   = 1 then i+1 else p.[i+1]
        Sto, Single a1
    | 4     -> 
        let a1 = if first   = 1 then p.[i+1] else p.[p.[i+1]]
        Ret, Single a1
    | 5 -> 
        let a1 = if first  = 1 then p.[i+1] else p.[p.[i+1]]
        let a2 = if second = 1 then p.[i+2] else p.[p.[i+2]]
        Jit, Tuple (a1,a2)
    | 6 -> 
        let a1 = if first  = 1 then p.[i+1] else p.[p.[i+1]]
        let a2 = if second = 1 then p.[i+2] else p.[p.[i+2]]
        Jif, Tuple (a1,a2)
    | 7 -> 
        let a1 = if first   = 1 then p.[i+1] else p.[p.[i+1]]
        let a2 = if second  = 1 then p.[i+2] else p.[p.[i+2]]
        let a3 = if third   = 1 then i+3 else p.[i+3]
        Lt, Triple(a1,a2,a3)
    | 8 -> 
        let a1 = if first   = 1 then p.[i+1] else p.[p.[i+1]]
        let a2 = if second  = 1 then p.[i+2] else p.[p.[i+2]]
        let a3 = if third   = 1 then i+3 else p.[i+3]
        Eq, Triple(a1,a2,a3)
    | 99    -> 
        Brk, Single -666
    | v     -> failwithf "getOp: Illegal instruction %d" v

let program = 
    File.ReadAllText "puzzle.in"
    |> fun s -> s.Split(',')
    |> Array.map int


let run p arg =
    let out = ref -666
    let rec loop i =
        match getOpArgs p i with
        | Add, Triple(v1,v2,v3) ->
            p.[v3] <- v1+v2
            loop (i+4)
        | Mul, Triple(v1,v2,v3) ->
            p.[v3] <- v1*v2
            loop (i+4)
        | Sto, Single v1 ->
            p.[v1] <- arg
            loop (i+2)
        | Ret, Single v1 ->
            out := v1
            printfn "%d" !out
            loop (i+2)
        | Jit, Tuple(v1,v2) ->
            if v1 <> 0 then v2 else i+3
            |> loop
        | Jif, Tuple(v1,v2) ->
            if v1 = 0 then v2 else i+3
            |> loop
        | Lt, Triple(v1,v2,v3) ->
            p.[v3] <- if v1 < v2 then 1 else 0
            loop (i+4)
        | Eq, Triple(v1,v2,v3) ->
            p.[v3] <- if v1 = v2 then 1 else 0
            loop (i+4)
        | Brk, _         -> !out
        | _ -> failwith "run: Illegal instruction"
    loop 0

run (Array.copy program) 5
    

