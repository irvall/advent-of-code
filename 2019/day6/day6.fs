open System.IO



type 'v stack =
    EmptyStack
    | Node of 'v * 'v stack

let empty = EmptyStack

let push v stack =
    match stack with
    | EmptyStack        -> Node (v, EmptyStack)
    | Node(_,s) as rs   -> Node(v, rs)

let pop stack =
    match stack with
    | EmptyStack -> failwith "pop: Cannot pop the empty stack"
    | Node(v,s)  -> (v,s)

let rec map f stack =
    match stack with
    | EmptyStack -> EmptyStack
    | Node(v,s)  -> Node(f v, map f s)

let fromList xs =
    List.fold(fun st v -> push v st) empty xs

let rec sum stack =
    match stack with
    | EmptyStack -> 0
    | Node(v,s)  -> v + sum s

let sumBy f stack =
    map f stack
    |> sum


////////////
// PART ONE
////////////
let update k v state =
    match Map.tryFind k state with
    | Some vs -> Map.add k (v::vs) state
    | None    -> Map.add k [v] state

let rec visit k d state =
    match Map.tryFind k state with
    | Some ps -> d + List.sumBy (fun k -> visit k (d+1) state) ps
    | None -> d

let answer =
    File.ReadAllLines "puzzle.in"
    |> Array.map (fun s -> s.Split(')')
        >> fun a -> (a.[0], a.[1]))
    |> Array.fold (fun state (k, v) -> 
        update k v state) Map.empty
    |> visit "COM" 0


// PART TWO

let update1 k v state =
    match Map.tryFind k state with
    | Some vs -> Map.add k (push v vs) state
    | None    -> Map.add k (push v empty) state

let rec visit1 k state =
    printfn "k=%A" k
    if k = "SAN" then 0
    else
    match Map.tryFind k state with
    | Some ps -> 
        let (v, stack1) = pop ps
        

    | None -> 0

let answer2 =
    File.ReadAllLines "part2.in"
    |> Array.map (fun s -> s.Split(')')
        >> fun a -> (a.[0], a.[1]))
    |> Array.fold (fun state (k, v) -> 
        update1 k v state
        |> update1 v k) Map.empty
    |> visit1 "YOU"
