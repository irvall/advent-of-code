open System.IO

let (wire1, wire2) = 
    File.ReadAllLines "small.input"
    |> Array.map (fun s -> s.Split(',')
    >> Array.collect (fun s -> 
        let v = int s.[1 ..] in
        match s.[0] with
        | 'R' -> List.map (fun v1 -> (v1,0)) [0 .. v] |> Array.ofList
        | 'L' -> List.map (fun v1 -> (v1,0)) [v .. -1 .. 0] |> Array.ofList
        | 'U' -> List.map (fun v1 -> (0,v1)) [0 .. v] |> Array.ofList
        | 'D' -> List.map (fun v1 -> (0,v1)) [v .. -1 .. 0] |> Array.ofList
        | _   -> failwith "Expected one of {R, L, U, D}"))
    |> fun arr -> List.ofArray arr.[0], List.ofArray arr.[1]

    

let travel xs state iden =
    let rec loop xs (x,y) state =
        match xs with
        | [] -> state
        | (xi,yi) :: xs -> 
            let p = (x+xi, y+yi)
            match Map.tryFind p state with
            | Some (v,id) when id <> iden -> loop xs p (Map.add p ((v+1), iden) state)
            | _                           -> loop xs p (Map.add p (1, iden) state)
    loop xs (0,0) state

let m1 = travel wire1 Map.empty 0
let m2 = travel wire2 m1 1
let R = 100000
Map.filter (fun (x,y) (v,id) -> v > 1 && (abs x < R && abs y < R)) m2