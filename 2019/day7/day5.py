import sys
from enum import Enum
program = [int(x) for x in open(sys.argv[1], 'r').read().split(',')]
print('prog=',program)

class Op(Enum):
    ADD = 1
    MUL = 2
    STORE = 3
    RET = 4
    JIT = 5
    JIF = 6
    LT = 7
    EQ = 8
    EXIT = 99
     


POS = 0
IMM = 1

def get_args(p, i, n):
    v = p[i]
    modes = [(v % 1000) // 100, (v % 10000) // 1000, v // 10000]
    print('v=', v)
    print('modes=', modes)
    args = []
    for j in range(n):
        k = i + j + 1
        print('k=',k)
        if modes[j] == POS:
            args.append(p[p[k]])
        elif modes[j] == IMM:
            args.append(p[k])
        else:
            raise ValueError("get_args: Wrong mode " + modes[j])
    
    print('1 args', args)
    return args

def run(p, i, args):
    print('running program with', args)
    p = p.copy()
    args = iter(args)
    while True:
        print('ip=',i)
        v = p[i]
        op = Op(v % 100)
        print(op)
        if op == Op.ADD:
            n = 3
            v1, v2, v3 = get_args(p, i, n)
            p[v3] = v1+v2
            i += (n+1)
        elif op == Op.MUL:
            n = 3
            v1, v2, v3 = get_args(p, i, n)
            p[v3] = v1*v2
            print('p=',p)
            i += (n+1)
        elif op == Op.STORE:
            n = 1
            v, = get_args(p, i, n)
            p[v] = next(args)
            i += (n+1)
        elif op == Op.RET:
            n = 1
            v, = get_args(p, i, n)
            i += (n+1)
            return (i,p[v])
        elif op == Op.JIT:
            n = 2
            v1, v2 = get_args(p, i, n)
            v3 = v2 if v1 != 0 else i+(n+1)
            i = v3
        elif op == Op.JIF:
            n = 2
            v1, v2 = get_args(p, i, 2)
            v3 = v2 if v1 == 0 else i+(n+1)
            i = v3
        elif op == Op.LT:
            n = 3
            v1,v2,v3 = get_args(p, i, 4)
            p[v3] = 1 if v1 < v2 else 0
            i += (n+1) 
        elif op == Op.EQ:
            n = 3
            v1,v2,v3= get_args(p, i, n)
            p[v3] = 1 if v1 == v2 else 0
            i += (n+1)
        elif op == Op.EXIT:
            print("\n### HALTING ###\n")
            return (i, None)
        else:
            raise ValueError("Didn't expect opcode:", op)
    return p

def trySeq(seq):
    print('seq=', seq)
    arg = 0
    pc = 0
    while arg != None: 
        for v in seq:
            args = [v, arg]
            (pc, arg) = run(pc, program, args) 
    return arg


if __name__ == "__main__":
    xargs = [int(x) for x in sys.argv[2:]]
    (v,a) = run(program, 0, xargs)
    print("RESULT ===", v)