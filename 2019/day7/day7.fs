open System.IO

let OpAdd = 1
let OPMUL   = 2
let OPSTO   = 3
let OPPRT    = 4
let OPJIT   = 5
let OPJIF   = 6
let OPLT    = 7
let OPEQ    = 8

let POS = 0
let IMM = 1

let getOp t =
    match t with
    | (_,_,_,_,op) -> op
    | _ -> failwith "getOp: Expected tuples of five"

let parse i =
    (i / 10000, i / 1000 % 10, i / 100 % 10, i % 100)

let program =
    File.ReadAllText "day5.in"
    |> fun s -> s.Split(',') 
    |> Array.map int


let run (p: int array) arg =
    let rec loop ip =
        if ip = 99999 then ()
        else
        let (third, second, first, op) = parse p.[ip]
        let (arg1, arg2, arg3) = p.[ip+1], p.[ip+2], p.[ip+3]
        match op with
        | 1 -> 
            let x = if first = POS then p.[arg1] else arg1
            let y = if second = POS then p.[arg2] else arg2
            if third = POS then p.[arg3] <- x + y else p.[ip+3] <- x+y
            loop (ip+4)
        | 2 ->
            let x = if first = POS then p.[arg1] else arg1
            let y = if second = POS then p.[arg2] else arg2
            if third = POS then p.[arg3] <- x * y else p.[ip+3] <- x*y
            loop (ip+4) 
        | 3 -> 
            if first = POS then p.[arg1] <- arg else p.[ip+1] <- arg
            loop (ip+2)
        | 4  ->
            let v = if first = POS then p.[arg1] else arg1
            printfn "%d" v
            loop (ip+2)
        | 5 ->
            let x = if first = POS then p.[arg1] else arg1
            let ip' =
                if x <> 0 then
                    if second = POS then p.[arg2] else arg2
                else ip + 3
            loop ip'
        | 6 ->
            let x = if first = POS then p.[arg1] else arg1
            let ip' =
                if x <> 0 then
                    if second = POS then p.[arg2] else arg2
                else ip + 3
            loop ip'
        | 7  ->
            let x = if first = POS then p.[arg1] else arg1
            let y = if second = POS then p.[arg2] else arg2
            let v =
                if x < y 
                then 1
                else 0
            if third = POS then p.[arg3] <- v else p.[ip+3] <- v
            loop (ip+4)
        | 8  ->
            let x = if first = POS then p.[arg1] else arg1
            let y = if second = POS then p.[arg2] else arg2
            let v =
                if x = y 
                then 1
                else 0
            if third = POS then p.[arg3] <- v else p.[ip+3] <- v
            loop (ip+4)
        | 99 -> ()
        | i -> failwithf "run: Illegal instruction %d" i
    loop 0
    p