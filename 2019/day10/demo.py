import sys, random
w = 21
h = 21

EMPTY = '.'
AST   = '#'
grid = [[EMPTY if random.uniform(0.0, 1.0) > 0.5 else AST for _ in range(w)] for _ in range(h)]

def pg():
    for g in grid:
        for c in g:
            print(c, end=' ')
        print()


def tag(x,y, sym = AST):
    grid[y][x] = sym

def isAst(x,y):
    return grid[x][y] == AST

def visit(area, x, y):
    startx, starty = x-(area//2), y-(area//2)-1
    print('startx', startx, 'starty', starty)
    astCount = 0
    ast = set()
    for i in range(area):
        starty += 1 
        if starty > h or starty < 0: continue
        for j in range(area):
            tx = startx+j
            if tx > w or tx < 0: continue
            tag(startx+j, starty, '-')
            if isAst(tx, starty): 
                astCount += 1
                ast.add((tx, starty))
    print('count =', astCount)
    return ast
    

def gcd(a,b):
    if b > a:
        return gcd(b, a)
    if b == 0: return a
    return gcd(b, a % b)
    
    


print(visit(*(int(x) for x in sys.argv[1:])))
pg()

print(gcd(10,16))