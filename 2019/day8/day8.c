#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int convert(int bin[], int start) {
    int ret = 0;
    for(int i = 0; i < start; i++) {
        int v = pow(2, start-i);
        ret += bin[i] ? v : 0;
    }
    return ret;
}

int main(int argc, char const *argv[]) {

    FILE *fp = fopen(argv[1], "r");
    size_t fileSize, imageSize;

    int w = atoi(argv[2]);
    int h = atoi(argv[3]);

    imageSize = w*h;

    if(!fp) {
        perror("Bummer, file opening failed!");
        return EXIT_FAILURE;
    }


    fseek(fp, 0L, SEEK_END);
    fileSize = ftell(fp);

    int* image = calloc(sizeof(int), imageSize);
    int* data  = calloc(sizeof(int), fileSize);
    int layerCount = fileSize / h;

    rewind(fp);
    
    for(int i = 0; i < fileSize; i++)
        data[i] = fgetc(fp)-'0';

    for(int i = 0; i < imageSize; i++) {
        int ti = i;             
        while(data[ti] == 2)
            ti += imageSize;
        image[i] = data[ti];
    }

    int k = 0;
    for(int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) 
            printf("%s", image[i * (j+i)] ? "###" : "   ");
        printf("\n");
    }

    0 1 2
    3 4 5
    6 7 8

    return 0;
}
