open System.IO

let masses = 
    File.ReadAllLines "day1_input.txt"
    |> Array.map int

let answer1  = Array.sumBy (fun v -> v / 3 - 2) masses

let calculate v =
    let rec loop v acc =
        if v < 8 then acc
        else 
        let sum = v / 3 - 2
        in loop sum (acc + sum)
    loop v 0

let answer2 = Array.sumBy calculate masses
